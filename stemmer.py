from nltk.stem import SnowballStemmer
import pymorphy2

snowball = SnowballStemmer(language="russian")

print(snowball.stem("Терпеть"))
print(snowball.stem("Терплю"))

morph = pymorphy2.MorphAnalyzer()

print(morph.parse("Терпеть"))
print(morph.parse("Терплю"))

from google_play_scraper import Sort, reviews_all
import pandas as pd
import csv

result = reviews_all(
    "com.idamob.tinkoff.android",
    sleep_milliseconds=0,
    lang="ru",
    country="ru",
    sort=Sort.NEWEST,
)

header = ["userName", "content", "replyContent", "score"]

with open("dataset.csv", "w", encoding="UTF8", newline="") as f:
    writer = csv.writer(f)
    writer.writerow(header)

    for row in result:

        writer.writerow(
            [row[header[0]], row[header[1]], row[header[2]], row[header[3]]]
        )

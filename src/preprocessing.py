import pandas as pd
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import pymorphy2
import csv


def lemmatize(text):
    words = text
    res = list()
    for word in words:
        p = morph.parse(word)[0]
        res.append(p.normal_form)

    return res


dataframe = pd.read_csv("dataset.csv")

#  Токенизация
texts = dataframe["content"]

tokenized_texts = [
    " ".join([w for w in word_tokenize(str(t)) if w.isalpha()]) for t in texts
]
tokenized_texts = [f.lower() for f in tokenized_texts]

#  Стоп-слова
stop_words = stopwords.words("russian")

tokenized_texts = [i.split(" ") for i in tokenized_texts]

filtered_review = list()
for i in tokenized_texts:
    filtered_review.append([word for word in i])

#  Лемматизация
morph = pymorphy2.MorphAnalyzer()
new_content = list()

for i in range(len(texts)):
    lemmatized_words = lemmatize(filtered_review[i])
    new_content.append(" ".join(str(x) for x in lemmatized_words if x not in stop_words))


header = ["userName", "content", "replyContent", "score"]

with open("preprocessed_dataset.csv", "w", encoding="UTF8", newline="") as f:
    writer = csv.writer(f)

    # write the header
    writer.writerow(header)
    cnt = 0
    for index, row in dataframe.iterrows():
        writer.writerow(
            [
                row[header[0]],
                new_content[cnt],
                row[header[2]],
                row[header[3]],
            ]
        )
        cnt += 1

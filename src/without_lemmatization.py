import pandas as pd
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import pymorphy2
import csv


dataframe = pd.read_csv("dataset.csv")

#  Токенизация
texts = dataframe["content"]

tokenized_texts = [
    " ".join([w for w in word_tokenize(str(t)) if w.isalpha()]) for t in texts
]
tokenized_texts = [f.lower() for f in tokenized_texts]

#  Стоп-слова
stop_words = stopwords.words("russian")

tokenized_texts = [i.split(" ") for i in tokenized_texts]

filtered_review = list()
for i in tokenized_texts:
    filtered_review.append([word for word in i])

new_content = list()

for i in range(len(texts)):
    new_content.append(" ".join(str(x) for x in filtered_review[i] if x not in stop_words))


header = ["userName", "content", "replyContent", "score"]

with open("preprocessed_dataset_without_lemmatization.csv", "w", encoding="UTF8", newline="") as f:
    writer = csv.writer(f)

    # write the header
    writer.writerow(header)
    cnt = 0
    for index, row in dataframe.iterrows():
        writer.writerow(
            [
                row[header[0]],
                new_content[cnt],
                row[header[2]],
                row[header[3]],
            ]
        )
        cnt += 1

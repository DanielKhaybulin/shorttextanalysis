import numpy as np
from scipy.spatial.distance import euclidean as euclidian_distance
from sklearn.cluster import DBSCAN
from sklearn.metrics import silhouette_score
from code_.clusterization import Clustering
from code_.instances import *
from code_.clusterization.visualize_clusters import *
from code_.clusterization.quality_metrics import *

class DBSCAN_Clustering(Clustering):
    def __init__(self, model, eps=0.25):
        self.name = Clusterization_Instances.DBSCAN
        self.model_name = model.name
        self.model = model
        self.corpus = self.compute_corpus()

        dbs = DBSCAN(eps=eps, min_samples=5).fit(self.corpus)

        self.labels_ = dbs.labels_
        self.clusterization_result_ = self.compute_clusterization_result()
        self.cluster_sizes_ = self.compute_cluster_sizes()
        self.centers_ = self.compute_centers()

    ''' helper tools '''
    def get_vec(self, ind):
        if self.model_name == Vectorization_Instances.TFIDF:
            return self.corpus[ind].todense()
        elif self.model_name == Vectorization_Instances.Word2Vec:
            return self.corpus[ind]

    ''' initialize fields '''
    def compute_corpus(self):
        if self.model_name == Vectorization_Instances.TFIDF:
            return self.model.vectors
        elif self.model_name == Vectorization_Instances.Word2Vec:
            return self.model.vectors
        elif self.model_name == Vectorization_Instances.Doc2Vec:
            raise NotImplemented("Later!")
        raise NameError("Unknown model")


    def compute_clusterization_result(self):
        clusterization_result_ = {i:[] for i in np.unique(self.labels_)}
        for i in range(len(self.labels_)):
            clusterization_result_[self.labels_[i]].append(i)
        return clusterization_result_

    def compute_cluster_sizes(self):
        return {i:len(cluster) for i, cluster in self.clusterization_result_.items()}

    def compute_centers(self):
        centers_ = {}
        for i, indices in self.clusterization_result_.items():
            points_of_cluster_i = [self.get_vec(j) for j in indices]
            centers_[i] = np.mean(points_of_cluster_i, axis=0)
        return centers_

    ''' quality metrics '''
    def average_incluster_distance(self, p=lambda x, y: euclidian_distance(x, y)):
        print(f'average_incluster_distance for {self.model_name.name}+DBSCAN = %.3f' % averageInclusterDistance(self, p))

    def intercluster_distance(self, p=lambda x, y: euclidian_distance(x, y)):
        print(f'intercluster_distance for {self.model_name.name}+DBSCAN = %.3f' % interclusterDistance(self, p))

    def silhouette_score(self):
        print(f'silhouette_score for {self.model_name.name}+DBSCAN = %.3f' % silhouette_score(self.corpus, self.labels_, metric='euclidean'))

    ''' visualization '''
    def show_cluster(self, cluster_index):
        showCluster(self, cluster_index)

    def show_as_TreeMap(self, topn=0):
        showClusterTreeMap(self, topn)

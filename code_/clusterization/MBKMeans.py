import numpy as np
from scipy.spatial.distance import euclidean as euclidian_distance
from sklearn.cluster import MiniBatchKMeans
from sklearn.metrics import silhouette_score
from code_.clusterization import Clustering
from code_.instances import *
from code_.clusterization.visualize_clusters import *
from code_.clusterization.quality_metrics import *


class MBKMeans_Clustering(Clustering):
    def __init__(self, model, k=20, mb=500):
        self.name = Clusterization_Instances.MBKMeans
        self.model_name = model.name
        self.model = model
        self.corpus = self.compute_corpus()

        km = MiniBatchKMeans(n_clusters=k, batch_size=mb).fit(self.corpus)

        self.labels_ = km.labels_
        self.clusterization_result_ = self.compute_clusterization_result()
        self.cluster_sizes_ = self.compute_cluster_sizes()
        self.centers_ = {i:center for i, center in enumerate(km.cluster_centers_)}

    ''' helper tools '''
    def get_vec(self, ind):
        if self.model_name == Vectorization_Instances.TFIDF:
            return self.corpus[ind].todense()
        elif self.model_name == Vectorization_Instances.Word2Vec:
            return self.corpus[ind]

    ''' initialize fields '''
    def compute_corpus(self):
        if self.model_name == Vectorization_Instances.TFIDF:
            return self.model.vectors
        elif self.model_name == Vectorization_Instances.Word2Vec:
            return self.model.vectors
        elif self.model_name == Vectorization_Instances.Doc2Vec:
            raise NotImplemented("Later!")
        raise NameError("Unknown model")

    def compute_clusterization_result(self):
        clusterization_result_ = {i:[] for i in np.unique(self.labels_)}
        for i in range(len(self.labels_)):
            clusterization_result_[self.labels_[i]].append(i)
        return clusterization_result_

    def compute_cluster_sizes(self):
        return {i:len(cluster) for i, cluster in self.clusterization_result_.items()}

    ''' quality metrics '''
    def average_incluster_distance(self, p=lambda x, y: euclidian_distance(x, y)):
        print(f'average_incluster_distance for {self.model_name.name}+MBKMeans = %.3f' % averageInclusterDistance(self, p))

    def intercluster_distance(self, p=lambda x, y: euclidian_distance(x, y)):
        print(f'intercluster_distance for {self.model_name.name}+MBKMeans = %.3f' % interclusterDistance(self, p))

    def silhouette_score(self):
        print(f'silhouette_score for {self.model_name.name}+MBKMeans = %.3f' % silhouette_score(self.corpus, self.labels_, metric='euclidean'))

    ''' visualization '''
    def show_cluster(self, cluster_index):
        showCluster(self, cluster_index)

    def show_as_TreeMap(self, topn=0):
        showClusterTreeMap(self, topn)


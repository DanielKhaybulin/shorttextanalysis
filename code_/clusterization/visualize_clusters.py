import matplotlib.pyplot as plt
import squarify

from code_.visualization import plotWithTSNE
from code_.instances import Vectorization_Instances


def showClusterTreeMap(model, topn):
    indices = sorted(model.cluster_sizes_, key=model.cluster_sizes_.get)[-topn:]
    labels = []
    for i in indices:
        if model.model_name == Vectorization_Instances.TFIDF:
            labels.append(
                f"Cluster №{i}\n{model.model.most_similar(positive=model.centers_[i])}\n{model.cluster_sizes_[i]}"
            )
        elif model.model_name == Vectorization_Instances.Word2Vec:
            labels.append(
                f"Cluster №{i}\n{model.model.most_similar(positive=model.centers_[i])}\n{model.cluster_sizes_[i]}"
            )
        elif model.model_name == Vectorization_Instances.Doc2Vec:
            '''
            labels.append(
                f"{model.model.most_similar(positive=np.asarray(model.centers_)[i], topn=1)[0]}\n{model.cluster_sizes_[i]}"
            )
            '''

    plt.figure(figsize=(16, 9))
    squarify.plot([model.cluster_sizes_[i] for i in indices], label=labels, alpha=0.8)

    plt.title(f"{model.model_name.name}+{model.name.name} Clustering Tree Map", fontsize=25, fontweight="bold")

    plt.axis("off")
    plt.show()


def showCluster(model, cluster_index):
    points_of_cluster_i = model.X[model.labels_ == cluster_index]
    title = f'''{model.model_name} + {model.name} Clusterization
                                        \n Cluster №{cluster_index}'''
    if model.model_name == Vectorization_Instances.Word2Vec or model.model_name == Vectorization_Instances.Doc2Vec:
        plotWithTSNE(points_of_cluster_i, title=title, color="black")
    elif model.model_name == Vectorization_Instances.TFIDF:
        plotWithTSNE(points_of_cluster_i, init="random",
                     n_iter=500, title=title, color="darkgreen")

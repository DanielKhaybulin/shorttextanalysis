def averageInclusterDistance(model, p):
    totsum = .0
    for i, indices in model.clusterization_result_.items():
        totsum += sum(
            map(
                lambda j: p(model.get_vec(i), model.centers_[i]),
                indices,
            )
        ) / model.cluster_sizes_[i]

    return totsum

def interclusterDistance(model, p):
    totsum = .0
    for i, center_i in model.centers_.items():
        for j, center_j in model.centers_.items():
            if i < j:
                totsum += p(center_i, center_j)

    return totsum

from code_.instances import *
from code_.clusterization.visualize_clusters import *
from code_.clusterization.quality_metrics import *


class Clustering:
    def __init__(self) -> None:
        self.name = None
        self.model_name = None
        self.model = None
        self.corpus = None
        self.labels_ = None
        self.clusterization_result_ = None
        self.cluster_sizes_ = None
        self.centers_ = None


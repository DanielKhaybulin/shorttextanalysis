from preprocess_sentences import prepare_sentences
from TFIDF import TFIDF_Model
from Word2Vec import vectorize, Word2Vec_Model
from clusterization import MBKMeans_Clustering, DBSCAN_Clustering

from gensim.matutils import corpus2csc


def main():
    sentences = prepare_sentences()
    tfidf_class = TFIDF_Model(corpus2csc(sentences).T.toarray())
    """
    w2v_class = Word2VecModel(sentences)
    w2v_vectorized = vectorize(sentences, w2v_class.w2v_model)
    mbkm = MBKMeans_Clustering(
        X=w2v_vectorized,
    )
    dbsc = DBSCAN_Clustering(
        X=w2v_vectorized,
    )
    mbkm.show_as_TreeMap()
    dbsc.show_as_TreeMap()
    """


if __name__ == "__main__":
    main()

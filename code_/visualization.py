import matplotlib.pyplot as plt
from sklearn.manifold import TSNE


def plotWithTSNE(vectors, init="pca", n_iter=2000, title=None, color="purple"):
    tsne_2d = TSNE(
        n_components=2, learning_rate=200, init=init, n_iter=n_iter, random_state=10
    )
    embeddings_ak_2d = tsne_2d.fit_transform(vectors)

    plt.figure(figsize=(16, 9))
    plt.grid(True)
    plt.title(title)

    x = embeddings_ak_2d[:, 0]
    y = embeddings_ak_2d[:, 1]
    plt.scatter(x, y, c=color, alpha=1)
    plt.show()

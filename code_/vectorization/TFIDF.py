import pandas as pd
import numpy as np

from scipy.spatial.distance import euclidean as euclidean_distance

from sklearn.feature_extraction.text import TfidfVectorizer
from code_.instances import Vectorization_Instances
from code_.visualization import plotWithTSNE


class TFIDF_Model:
    '''
    tf-idf as vectorization
    requires using cosine metric for clustering,
    when MiniBatchKMeans and DBSCAN both use euclidean.
    However, for l2-normalized vectors
    cosine and euclidean distances are connected linearly.
    Thus we go on.
    '''
    def __init__(self, corpus, max_features=1000):
        self.name = Vectorization_Instances.TFIDF
        self.corpus = corpus

        self.model = TfidfVectorizer(min_df=1,
                                     max_df=0.95,
                                     max_features=max_features)
        self.vectors = self.model.fit_transform(corpus)

    def get_top_keywords(self, labels, n_terms):
        df = pd.DataFrame(self.vectors.todense()).groupby(labels).mean()

        features = self.model.get_feature_names_out()
        return [[features[t] for t in np.argsort(row)[-n_terms:]] for _, row in df.iterrows()]

    def most_similar(self, positive):
        p = euclidean_distance
        ind_closest = min(range(self.vectors.shape[0]), key=lambda i: p(self.vectors.getrow(i).todense(), positive))

        return self.corpus[ind_closest]

    def plot_with_TSNE(self):
        plotWithTSNE(self.vectors, init="random", n_iter=500,
                     title=f"{self.name.name} Vectorization", color="tomato")

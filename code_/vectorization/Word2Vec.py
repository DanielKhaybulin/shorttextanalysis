import numpy as np
from gensim.models import Word2Vec
from scipy.spatial.distance import cosine as cosine_distance
from code_.instances import Vectorization_Instances
from code_.visualization import plotWithTSNE


def vectorize_sentences(corpus):
    model = Word2Vec(
        min_count=5,
        window=3,
        vector_size=500,
        sample=6e-5, 
        alpha=0.03, 
        min_alpha=0.0007, 
        negative=20,
        workers=-1
    )
    model.build_vocab(corpus, progress_per=10000)
    model.train(
        corpus,
        total_examples=model.corpus_count,
        epochs=30,
        report_delay=1
    )
    model.init_sims(replace=True)

    sent2vec = {}
    vec2sent = {}
    for sent in corpus:
        sent_vec = np.zeros(model.vector_size)
        for word in sent:
            try:
                sent_vec += model.wv[word]
            except:
                print("Error") # shouldn't get into here

        complete_sentence = ' '.join(sent)
        sent_vec_as_tuple = tuple(sent_vec)
        sent2vec.update({complete_sentence : sent_vec_as_tuple})
        vec2sent.update({sent_vec_as_tuple : complete_sentence})
    return sent2vec, vec2sent

class Word2Vec_Model:
    def __init__(self, corpus):
        self.name = Vectorization_Instances.Word2Vec
        self.corpus = corpus

        self.sent2vec, self.vec2sent = vectorize_sentences(corpus)
        self.vectors = list(self.vec2sent.keys())

    def most_similar(self, positive):
        p = cosine_distance
        closest = min(self.vectors, key=lambda x: p(x, positive))
        return self.vec2sent.get(closest)

    def plot_with_TSNE(self):
        plotWithTSNE(self.vectors,
                     title=f"{self.name.name} Vectorization")

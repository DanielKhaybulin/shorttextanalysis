import numpy as np

from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from code_.instances import Vectorization_Instances
from code_.visualization import plotWithTSNE

def tagged_document(lolowords):
    for i, lowords in enumerate(lolowords):
        yield TaggedDocument(lowords, [str(i)])

class Doc2Vec_Model:
    def __init__(self, sentences):
        self.name = Vectorization_Instances.Doc2Vec
        self.sentences = sentences
        self.corpus = list(tagged_document(sentences))
        self.model = Doc2Vec(
            min_count=2,
            window=5,
            vector_size=300,
            sample=6e-5,
            alpha=0.03,
            min_alpha=0.0007,
            negative=20,
            workers=-1,
        )
        self.model.build_vocab(self.corpus, progress_per=10000)
        self.model.train(
            self.corpus,
            total_examples=self.model.corpus_count,
            epochs=30,
            report_delay=1,
        )
        # self.model.init_sims(replace=True)

    def most_similar(self, positive, topn=10):
        return self.model.wv.most_similar(positive=positive, topn=topn)

    def plot_with_TSNE(self):
        plotWithTSNE(self.model.wv.vectors,
                     title=f"{self.name.name} Vectorization")

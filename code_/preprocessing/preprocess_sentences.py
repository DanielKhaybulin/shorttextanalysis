import pandas as pd

def prepare_corpus(path_to_dataset):
    df = pd.read_csv(path_to_dataset)
    df.dropna(inplace=True)
    return df["content"]

def prepare_sentences(path_to_dataset):
    df = pd.read_csv(path_to_dataset)
    df.dropna(inplace=True)
    return [row.split() for row in df["content"]]

def prepare_list_of_sentences(path_to_dataset):
    df = pd.read_csv(path_to_dataset)
    df.dropna(inplace=True)
    return [row for row in df["content"]]

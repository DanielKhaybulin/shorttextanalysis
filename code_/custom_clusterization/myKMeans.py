from IPython.display import clear_output
from sklearn.metrics import pairwise_distances_argmin, pairwise_distances
import random
import numpy as np
import matplotlib.pyplot as plt



def plot_clust(X, centers, best_centers, lables, ax):
    ax.scatter(X[:, 0], X[:, 1], c=lables)
    ax.scatter(centers[:, 0], centers[:, 1], marker='>', color='red')
    ax.scatter(best_centers[:, 0], best_centers[:, 1],
               marker='>', color='green')
    plt.show()


class MyKMeans():
    def __init__(self, n_clusters=3, n_iters=100, seed=None):
        self.n_clusters = n_clusters
        self.labels = None
        self.centers = None
        self.n_iters = n_iters
        self.seed = 0 if seed is None else seed
        np.random.seed(self.seed)

    def update_centers(self, X):
        centers = []
        for i in np.unique(self.labels):
            centers.append(np.mean(X[self.labels == i], axis=0))
        return np.array(centers)

    def update_lables(self, X):
        centroids = random.sample(range(len(X)), self.n_clusters)
        return np.array(pairwise_distances_argmin(X, X[centroids]))

    def fit(self, X):
        X = np.array(X)
        self.labels = self.update_lables(X)
        self.centers = self.update_centers(X)
        min_sum = -1

        for it in range(self.n_iters):
            new_labels = self.update_lables(X)
            self.labels = new_labels

            new_centers = self.update_centers(X)
            print(len(self.centers), len(new_centers))
            if np.allclose(self.centers.flatten(), new_centers.flatten(), atol=1e-1):
                self.centers = new_centers
                self.labels = new_labels
                print('Converge by tolerance centers')

                fig, ax = plt.subplots(1, 1, figsize=(20,14))
                plot_clust(X, new_centers, self.centers, new_labels, ax)
                return self
            sum = 0
            for i in np.unique(self.labels):
                sum += np.sum(pairwise_distances(
                    X=X[self.labels == i], Y=[new_centers[i]]))

            if min_sum == -1 or sum < min_sum:
                self.centers = new_centers
                min_sum = sum

            fig, ax = plt.subplots(1, 1, figsize=(20,14))
            plot_clust(X, new_centers, self.centers, new_labels, ax)
            plt.pause(0.3)
            clear_output(wait=True)

        return self

    def predict(self, X):
        return np.array(pairwise_distances_argmin(X, self.centers))

import numpy as np
import random
from scipy.spatial.distance import cdist
import matplotlib.pyplot as plt


class MyForel:
    def __init__(self, X, R):
        self.X = X
        self.R = R
        self.result = None
        self.clusters = None
    
    @staticmethod
    def get_mass_center(objects):
        return sum(objects) / len(objects)

    @staticmethod
    def distance_objects(obj1, obj2):
        obj1 = np.asarray(tuple(obj1)).reshape(1, -1)
        obj2 = np.asarray(tuple(obj2)).reshape(1, -1)

        return cdist(obj1, obj2, 'euclidean')[0][0]
    
    def get_random_obj(self):
        if len(self.X) == 0:
            return None
        
        if len(self.X) == 1:
            return self.X
        
        idx = random.randint(0, len(self.X) - 1)
        rand_obj = self.X[idx]
        self.X = np.delete(self.X, idx, 0)

        return rand_obj
    
    def clustering_not_finish(self):
        return len(self.X) != 0
    
    def get_same_objects(self, obj):
        same_objects = []
        i = 0
        idxs = []
        for row in self.X:
            if self.distance_objects(obj, row) <= self.R:
                same_objects.append(list(row))
                idxs.append(i)
            i += 1
        
        return [idxs, np.asarray(same_objects)]
    
    def remove_objects(self, same_obj_idxs):
        self.X = np.delete(self.X, same_obj_idxs, 0)
    
    def fit(self):
        self.clustered_objects = []
        
        while self.clustering_not_finish():
            cur_obj = self.get_random_obj()

            same_objects_info = self.get_same_objects(cur_obj)
            same_objects_idxs, same_objects = same_objects_info[0], same_objects_info[1]

            if len(same_objects) == 0:
                center_obj = cur_obj
            else:
                center_obj = self.get_mass_center(same_objects)
            
            while self.distance_objects(cur_obj, center_obj) != 0:
                cur_obj = center_obj
                same_objects_info = self.get_same_objects(cur_obj)
                same_objects_idxs, same_objects = same_objects_info[0], same_objects_info[1]
                center_obj = self.get_mass_center(same_objects)
            
            self.remove_objects(same_objects_idxs)

            self.clustered_objects.append(same_objects_info[1])
        
        self.result = []
        self.clusters = []
        
        for cluster in range(len(self.clustered_objects)):
            for row in self.clustered_objects[cluster]:
                self.result.append(list(row))
                self.clusters.append(cluster)
    
    def plot(self, col1, col2):
        X = np.array(self.result)[:, col1]
        Y = np.array(self.result)[:, col2]
        cs = [self.clusters[i] for i in range(len(self.clusters))]
        plt.scatter(X, Y, c=cs, s=50)
        plt.xlim(0)
        plt.ylim(0)
        plt.title("forel clusterization")
        plt.show()
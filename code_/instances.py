from enum import Enum


class Vectorization_Instances(Enum):
    TFIDF = 1,
    Word2Vec = 2,
    Doc2Vec = 3,


class Clusterization_Instances(Enum):
    MBKMeans = 1,
    DBSCAN = 2,
    Random = 3,
